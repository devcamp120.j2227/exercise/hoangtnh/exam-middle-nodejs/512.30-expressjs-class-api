const UserData = require("../data/data");
//get all users
const getAllUser = (request, response,next) => {
    const userAge = request.params.userAge;
    if(userAge){
        const users = UserData.filter( item =>
             item.age > userAge )
        response.json({
            message:`Get user with age greater than ${userAge} `,
            user: users
            })        
    }
    else{
         response.json ({
            message:"Get all users successfully",
            users : UserData
        })
    }
    next();
}

//function get user by params ID
const getUserById = (request, response,next) =>{
    const userId = request.params.userId;
    
    if(isNaN(userId) || userId<0 || userId >10){
        return response.status(400).json({
            status:"Bad request",
            message:"User Id phải là số & User Id không hợp lệ"
        })
    }
    
    response.json({
        status:`Get user by id ${userId} successfully`,
        user: UserData.filter((item) => item.id == userId )
    })
    
    next();
}
//function create new user
const createUser = (request,response,next) => {
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    //B2: validate dữ liệu
    if(isNaN(body.id) || !body.id){
        return response.status(400).json({
            status:"Bad request",
            message:"User Id không hợp lệ"
        })
    }
    if(!body.name){
        return response.status(400).json({
            status:"Bad request",
            message:"Name không hợp lệ"
        })
    }
    if(!body.position){
        return response.status(400).json({
            status:"Bad request",
            message:"Position không hợp lệ"
        })
    }
    if(!body.office){
        return response.status(400).json({
            status:"Bad request",
            message:"Office không hợp lệ"
        })
    }
    if(!body.age || isNaN(body.age)){
        return response.status(400).json({
            status:"Bad request",
            message:"Age không hợp lệ"
        })
    }
    if(!body.startDate){
        return response.status(400).json({
            status:"Bad request",
            message:"Start Date không hợp lệ"
        })
    }

    //B3: tạo dữ liệu
    const lastId = UserData.pop().id
    const newUser = {
        id: lastId + 1 || 1,
        name: body.name,
        position: body.position,
        office: body.office,
        age: body.age,
        startDate: body.startDate
    }

    UserData.push(newUser)
    return response.status(201).json({
        message: 'Create a new user successfully!',
        data: newUser
    })
    next();
}
// function Update user by id
const updateUserById = (request, response, next) => {
    const userId = request.params.userId
    const body = request.body

    if(!userId.trim() || isNaN(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User id không hợp lệ"
        })
    }

    if(!body.name) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Name không hợp lệ"
        })
    }

    if(!body.position.trim()) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Position không hợp lệ"
        })
    }

    if(!body.office.trim()) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Office không hợp lệ"
        })
    }

    if(isNaN(body.age)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Age không hợp lệ"
        })
    }

    if(!body.startDate.trim()) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Start date không hợp lệ"
        })
    }

    const userIndex = UserData.findIndex(user => user.id == userId)
    const updateUser = UserData[userIndex]
    if(body.name !== undefined){
        updateUser.name = body.name
    }
    if(body.position !== undefined) {
        updateUser.position = body.position
    }
    if(body.office !== undefined) {
        updateUser.office = body.office
    }
    if(body.age !== undefined) {
        updateUser.age = body.age
    }
    if(body.startDate !== undefined) {
        updateUser.startDate = body.startDate
    }

    return response.status(200).json({
        message: 'Update a user by id successfully!',
        data: updateUser
    })
    next();
}
// function Delete user by id
const deleteUserById = (request, response,next) => {
    const userId = request.params.userId

    if(!userId.trim() || isNaN(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User id không hợp lệ"
        })
    }

    const userIndex = UserData.findIndex(user => user.id == userId)
    UserData.splice(userIndex, 1)

    return response.status(200).json({
        message: 'Delete a user by id successfully!'
    });
    next();
}
module.exports = {
    getAllUser,
    getUserById,
    createUser,
    updateUserById,
    deleteUserById
}