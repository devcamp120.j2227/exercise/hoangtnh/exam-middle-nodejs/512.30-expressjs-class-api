//import express js
const express = require("express");

//Khai báo router
const router = express.Router();

//import middleware
const browserMiddleware = require("../middleware/browserMiddleware");

router.get("/", browserMiddleware.getCurrentTime);
router.get("/", browserMiddleware.getUrl);

module.exports = router;
