const express = require ("express");
const router = express.Router();

const userMiddleware = require("../middleware/userMiddleware");
const { route } = require("./browserRouter");
router.get("/users/ages/:userAge", userMiddleware.getAllUser);
router.get("/users/",userMiddleware.getAllUser);
router.get("/users/:userId",userMiddleware.getUserById);
router.post("/users", userMiddleware.createUser);
router.put("/users/:userId", userMiddleware.updateUserById);
router.delete("/users/:userId", userMiddleware.deleteUserById);
module.exports = router;