import { Student } from "./student.js";
class CollegeStudent extends Student {
    constructor(paramSpecialization, paramStudentCode ){
        super(),
        this.specialization = paramSpecialization,
        this.studentCode = paramStudentCode
    }
}
export {CollegeStudent};