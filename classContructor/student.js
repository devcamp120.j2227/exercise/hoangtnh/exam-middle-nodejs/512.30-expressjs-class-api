import { People } from "./people.js";
class Student extends People {
    constructor (paramSchool, paramClass, paramPhone){
        super(),
        this.school = paramSchool,
        this.class = paramClass,
        this.phone = paramPhone
    }
}
export {Student};