import { People } from "./people.js";
class Worker extends People {
    constructor (paramJob, paramWorkPlace, paramSalary){
        super(),
        this.job = paramJob,
        this.workPlace = paramWorkPlace,
        this.salary = paramSalary
    }
}
export {Worker};