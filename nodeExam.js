//import thư viện express JS
const express = require("express");

//khởi tạo app 
const app = express() ;

//import thư viện path
const path = require ("path");
//Khai báo cổng chạy
const port = 8000;

//Khai báo router
const browserRouter = require ("./app/routes/browserRouter");
const userRouter = require ("./app/routes/userRouter");

// Cấu hình request đọc được body json
app.use(express.json());

//app sử dụng router
app.use("/", browserRouter);
app.use("/api", userRouter);

//call api chạy browers
app.get("/", (request, response) => {
    response.sendFile(path.join(__dirname + "/view/projectJs.html"));
});



//Chạy app trên cổng đã khai báo
app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})
